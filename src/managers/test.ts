//----------------------------------------------------------------------------------------------------------------------
// TestManager
//----------------------------------------------------------------------------------------------------------------------

// Interfaces
import { PointlessThingie } from '../interfaces/test';

// Engines
import { validateAllTheThings } from '../engines/testValidation';

// ResourceAccess
import testRA from '../resource-access/test';

//----------------------------------------------------------------------------------------------------------------------

class TestManager
{
    async doStuff(pointlessThings : PointlessThingie[]) : Promise<Record<string, unknown>>
    {
        // Validate
        await validateAllTheThings(pointlessThings);

        // Add any potential goats
        await Promise.all(pointlessThings.map(async (thing) =>
        {
            return testRA.addGoat(thing);
        }));

        console.log('Total Goats:', await testRA.getTotalGoats());

        // Return stuff
        return {
            message: 'Stuff done. (Probably.)',
            goats: await testRA.getTotalGoats()
        };
    } // end doStuff
} // end TestManager

//----------------------------------------------------------------------------------------------------------------------

export default new TestManager();

//----------------------------------------------------------------------------------------------------------------------
