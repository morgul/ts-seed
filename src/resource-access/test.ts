//----------------------------------------------------------------------------------------------------------------------
// TestResourceAccess
//----------------------------------------------------------------------------------------------------------------------

// Interfaces
import { PointlessThingie } from '../interfaces/test';

// Utils
import { add } from '../utils/misc';

//----------------------------------------------------------------------------------------------------------------------

class TestResourceAccess
{
    #totalGoatsSeen = 0;

    async addGoat(thing : PointlessThingie) : Promise<void>
    {
        if(thing.isGoat)
        {
            this.#totalGoatsSeen = add(this.#totalGoatsSeen, 1);
        } // end if
    } // end addGoats

    async getTotalGoats() : Promise<number>
    {
        return this.#totalGoatsSeen;
    }
} // end TestResourceAccess

//----------------------------------------------------------------------------------------------------------------------

export default new TestResourceAccess();

//----------------------------------------------------------------------------------------------------------------------
