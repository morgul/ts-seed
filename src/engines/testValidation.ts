// ---------------------------------------------------------------------------------------------------------------------
// Test Validation Engine
// ---------------------------------------------------------------------------------------------------------------------

import { PointlessThingie } from '../interfaces/test';

// ---------------------------------------------------------------------------------------------------------------------

export async function validateAllTheThings(things : PointlessThingie[]) : Promise<void>
{
    if(things.length === 0)
    {
        throw new Error('No things to validate!');
    } // end if

    // Wait a sec
    return new Promise((resolve) => setTimeout(resolve, 1000));
} // end validateAllTheThings

// ---------------------------------------------------------------------------------------------------------------------
